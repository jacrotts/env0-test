terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.6.0"
    }
    env0 = {
      source  = "env0/env0"
      version = ">= 0.2.28"
    }
  }
}

provider "aws" {
  region = var.region
}
