module "demo-vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 3.1.0"

  name = var.name
  cidr = var.cidr

  enable_nat_gateway = var.enable_nat_gateway
  single_nat_gateway = true
  one_nat_gateway_per_az = false


  azs             = ["${var.region}a", "${var.region}b"]
  private_subnets = var.private_subnets
  public_subnets  = var.public_subnets
}

resource "env0_configuration_variable" "vpc_id" {
  name         = "vpc_id"
  project_id   = var.project_id
  value        = module.demo-vpc.vpc_id
  is_read_only = true
  type         = "terraform"
}
