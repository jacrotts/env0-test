terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.6.0"
    }
  }
}

provider "aws" {
  region = var.region
}
variable "region" {
  default = "us-west-2"
}

variable "vpc_id"{
  default = "default"
}

// variable "ec2_subnet" {
//   default = "default"
// }

data "aws_vpc" "main" {  
      id = var.vpc_id
    }

data "aws_subnets" "main" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.main.id]
  }
}
// data "aws_subnet_ids" "main" {  
//     vpc_id = data.aws_vpc.main.id
//     }


data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "web" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t3.micro"
  subnet_id = element(tolist(data.aws_subnets.main.ids), 0)

  tags = {
    Name = "HelloWorld"
  }
}
